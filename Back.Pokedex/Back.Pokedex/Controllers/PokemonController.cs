﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Back.Pokedex.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PokemonController : ControllerBase
    {

        [HttpGet]
        public IEnumerable<Pokemon> Get()
        {
            List<Pokemon> pokemons = new List<Pokemon>();

            PokeProvider provider = new PokeProvider();

            pokemons = provider.Listar();

            return pokemons;
        }

        
        [HttpGet("{id}")]
        public Pokemon Get(int id)
        {
            List<Pokemon> pokemons = new List<Pokemon>();
            PokeProvider provider = new PokeProvider();

            pokemons = provider.Listar();

            return pokemons.Where(p=> p.Id == id).FirstOrDefault();
        }

    }
}
