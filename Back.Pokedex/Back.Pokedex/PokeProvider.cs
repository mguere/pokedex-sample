﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Back.Pokedex
{
    public class PokeProvider
    {
        public List<Pokemon> Listar()
        {
            List<Pokemon> pokemons = new List<Pokemon>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://raw.githubusercontent.com/fanzeyi/pokemon.json/master/pokedex.json");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                //StringContent sc = new StringContent(null, Encoding.UTF8, "application/json");

                response = client.GetAsync("").Result;
                
                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    pokemons = JsonConvert.DeserializeObject<List<Pokemon>>(message.Result);                    
                }
            }

            foreach (var item in pokemons)
            {
                item.IdImage = item.Id.ToString().PadLeft(3, '0') + ".png";
            }

            return pokemons;
        }
    }
}
