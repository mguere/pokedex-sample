﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Back.Pokedex
{
    public class Pokemon
    {
        public Pokemon()
        {
            Name = new PokeName();
            Base = new PokeBase();
        }

        public int Id { get; set; }
        
        public string IdImage { get; set; }
        
        [JsonProperty("name")]
        public PokeName Name { get; set; }
        
        public List<string> Type { get; set; }

        [JsonProperty("base")]
        public PokeBase Base { get; set; }

    }


    public class PokeName
    {
        public string English { get; set; }
        public string Japanese { get; set; }
        public string Chinese { get; set; }
        public string French { get; set; }
    }

    public class PokeBase
    {
        [JsonProperty("HP")]
        public int HP { get; set; }
        [JsonProperty("Attack")]
        public int Attack { get; set; }
        
        [JsonProperty("Defense")]
        public int Defense { get; set; }
        
        [JsonProperty("Sp. Attack")]
        
        public int SpAttack { get; set; }

        [JsonProperty("Sp. Defense")]
        public int SpDefense { get; set; }

        [JsonProperty("Speed")]
        public int Speed { get; set; }
    }
}
