import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PokemonItemComponent } from './pokemon-item/pokemon-item.component';
import { PokemonListaComponent } from './pokemon-lista/pokemon-lista.component';

const routes: Routes = [

  {path:"pokemons", component: PokemonListaComponent},
  {path:"pokemons/:id", component: PokemonItemComponent},
  { path: '**' , pathMatch: 'full', redirectTo: 'pokemons'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
