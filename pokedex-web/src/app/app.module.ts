import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonItemComponent } from './pokemon-item/pokemon-item.component';
import { PokemonListaComponent } from './pokemon-lista/pokemon-lista.component';

@NgModule({
  declarations: [
    AppComponent,
    PokemonItemComponent,
    PokemonListaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
