import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';

@Injectable({
  providedIn: 'root'
})
export class PokedexService {

  constructor() { }

  get<T>(url: any, type?: any): Observable<any> {
    const headers = {'Content-Type': 'application/json'};
    const req = ajax.get(url, headers);
    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  listarPokemones(datos?:any): Observable<any> {
    return this.get("http://localhost:64188/api/pokemon/");
  }

  obtenerPokemon(datos?:any): Observable<any>{
      
    return this.get("http://localhost:64188/api/pokemon/" + datos);
  }

}
