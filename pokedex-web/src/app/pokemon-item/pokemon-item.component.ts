import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokedexService } from '../pokedex.service';

@Component({
  selector: 'app-pokemon-item',
  templateUrl: './pokemon-item.component.html',
  styleUrls: ['./pokemon-item.component.css']
})
export class PokemonItemComponent implements OnInit {

  idPokemon : number = 0;
  pokemon : any;

  constructor(private route : ActivatedRoute, private servicio : PokedexService ) { }

  ngOnInit(): void {
    this.idPokemon = this.route.snapshot.params.id;
    this.obtenerPokemonItem();
  }

  obtenerPokemonItem() {
    
    this.servicio.obtenerPokemon(this.idPokemon).subscribe((result: any)=> {
      
      this.pokemon = result.response;
      console.log(this.pokemon);
      
    }, (err: any)=> {console.log(err)});

  }

}
