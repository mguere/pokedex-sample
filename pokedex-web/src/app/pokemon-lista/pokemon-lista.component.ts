import { Component, OnInit } from '@angular/core';
import { PokedexService } from '../pokedex.service';

@Component({
  selector: 'app-pokemon-lista',
  templateUrl: './pokemon-lista.component.html',
  styleUrls: ['./pokemon-lista.component.css']
})
export class PokemonListaComponent implements OnInit {
  title = 'pokedex-web';
  pokemons! : any[];

  constructor(private service: PokedexService) { }

  ngOnInit(): void {
    this.cargarPokemones();
  }

  cargarPokemones():void{
    this.service.listarPokemones().subscribe((result: any)=> {
      console.log(result);

      this.pokemons = result.response;
      console.log(this.pokemons);
      
    }, (err: any)=> {console.log(err)});
  }

}
